---
layout: handbook-page-toc
title: "Security OKRs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Security OKRs
The Security organization executes quarterly OKRs according to the [Engineering OKR process](https://about.gitlab.com/handbook/engineering/#engineering-okr-process).

### How We Plan, Assign, and Execute Work

OKRs are docuemnted in epics. Any work that is related to an OKR is tracked with an issue(s) in the appropriate team or project tracker and linked to the OKR epic.

Larger initiatives that span the scope of multiple teams or projects may require a [Planning](./planning) handbook page to further developer requirements.

### Security OKR Epic Template Code 

:scissors: Copy and paste the below into your OKR Epic


```
<!-- Name this epic: FY22Q[#] OKR - [CEO Strategic Category] - [Measurable Title] => 0%  -->

## Objective: 
`what is the desired outcome of this OKR and how does it align with organizational strategy? How will you measure success?`
### Security Roadmap: 
`what security roadmap item does this OKR support`  
### Dependencies:  
* [ ] `dependency description` (tag stakeholder)
* [ ] `dependency description` (tag stakeholder)
* [ ] `dependency description` (tag stakeholder)
## Due Date:
`commonly end of quarter`
## Budget: 
`Any budget required to execute this OKR?`
## OKR DRI:
`Who is the DRI for this OKR? Generally a manager/distinguished/director role`

#### Additional Instructions
- Add the team label to indicate the team responsible for the OKR, example `~Security Management::TEAM NAME`
- Add `~FY22 Q#~` to indicate the quarter this OKR will be executed
- Relate Key Result (KR) Issues to the Epic
- Complete `Assigned to` `Start Date` `Due Date` in the epic sidebar 

/label ~"OKR" ~
```

### Security OKR Weekly Update Template Code

```
## Weekly update
`On Schedule/At Risk/Behind Schedule`

## What was accomplished
`What did you accomplish this week?`

## Next steps
`What will you work on next week?`
```

### Security KR Weekly Update Template Code

```
## Weekly KR Update for the week of YYYY-MM-DD
Status: :white_check_mark: - On Track, :warning: - Requires Escalation, :octagonal_sign: - Blocked (Leave the relevant status, remove the rest and this note)
Percentage complete: XX%

## Completed
* (List of what tasks were completed since last update)

## Next steps
* (List of what needs to happen in the next 1-2 weeks)

```
