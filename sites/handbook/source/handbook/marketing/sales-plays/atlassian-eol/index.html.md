---
layout: markdown_page
title: "Sales Play: Atlassian Server EOL"
description: "This page has all the information for the Atlassian Server EOL sales play."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Atlassian Server EOL Sales Play

**[View this page in a Google Doc](https://docs.google.com/document/d/1ZTYFGDJN6t2fJ824kt3BvvicN3-qr0U5afVSGXubEzE/edit?usp=sharing)**

### Context
- Atlassian has announced that they are ending support for Atlassian server products (see [this page](https://www.atlassian.com/migration/journey-to-cloud))
- This announcement is prompting some Atlassian customers to re-evaluate their SDLC toolchain
- You should try to find out which of your prospects and customers are currently using Atlassian server products and ask:
    - What concerns, if any, does the customer have about Atlassian's decision across different stages of the SDLC (or just general concerns)? 
    - Does this announcement impact the customer's instance of Jira, BitBucket (SCM), and/or Bamboo (CI)?
    - Would they like to learn more about why so many organizations are selecting GitLab to streamline their software development and delivery workflows and explore how we may be able to extend similar benefits to their organization?

### Potential Opportunities

1. They want to move off of BitBucket
2. They want to move off of Bamboo CI (or other e.g. Jenkins, TravisCI, CircleCI, etc.)
3. Pitch the value of a complete DevOps solution delivered as a single application
4. We integrate with Jira and/or can serve as an alternative if the customer is looking for a Jira alternative (this one is the most difficult since Jira is very “sticky” and feature-robust)

### Part 1: Before the Discovery Call

| Steps | Resources |
| ------ | ------ |
| 1. Familiarize yourself with these resources | - [Atlassian Server EOL Opportunity](https://www.atlassian.com/migration/journey-to-cloud)<br> - [GitLab vs. Atlassian Battles](https://drive.google.com/file/d/18H73Fkh-cbPd5M-z0hMuGOx_onWIhSo4/view?usp=sharing) (watch at least the first 10 minutes)<br> - [GitLab vs. Atlassian Comparison Deck](https://docs.google.com/presentation/d/1ydOqmyQmg0WFlmexPHxoPArv3SM5iFmX__0dV4HD0o0/edit?usp=sharing)<br> - [Mid Market Example Deck](https://docs.google.com/presentation/d/15zyK_SN7S_eBfzkqOH4BTHd-M452ITFHs5RFtpj79L8/edit?usp=sharing) |
| 2. Talk to the right persona | - The **CIO** is usually the **economic buyer**<br> - The **App Dev [Exec](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc) / [Director](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director) / [Manager](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager)** is interested in functionality and can **champion** GitLab internally |
| 3. Review discovery questions, objections, and differentiators | - See [this doc](https://docs.google.com/document/d/1KDb6wHjanyC805OD1my60wH9Yh5qaq5895KOKDVazXY/edit) (GitLab internal only) |
| 4.  Create a custom pitch deck | - Use [these slides](https://docs.google.com/presentation/d/1ydOqmyQmg0WFlmexPHxoPArv3SM5iFmX__0dV4HD0o0/edit?usp=sharing) |


### Part 2: The Discovery Call

| Steps | Resources |
| ------ | ------ |
| 1. Identify business objectives and priorities during discovery | These could be:<br> - One platform for easier collaboration across the organization and especially with a distributed workforce<br> - Cost savings<br> - Ability to eliminate multiple tools in favor of GitLab (and ability to seamlessly integrate if they don’t want to completely rip and replace)<br> - Higher levels of security and compliance to avoid breaches and associated risk |
| 2. Send a follow up email | [Email template](https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit?usp=sharing) to include:<br> - Any information they requested (resources you might include)<br> - How that information can be helpful for their research<br> - An offer to continue researching these topics on their behalf<br> - Mutually agreed upon next steps |


### Part 3: POV & Evaluation

| Steps | Resources |
| ------ | ------ |
| 1. Set up the evaluation with the appropriate internal resources | - N/A |
| 2. Do the following before sending out a trial key | - Mutually agree upon **success criteria** for the 30-day evaluation period (“If at the end of 30 days, we prove [these criteria], then we will consider this trial a success)”<br><br>Success criteria examples:<br> - Establish end to end DevSecOps workflow that improves cycle time and reduces Security & Compliance risk<br> - If self-managed installation, ensure stable and scalable infrastructure |
| 3. Send an email outlining and confirming the success criteria | - [Email template](https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit#bookmark=id.5grp5yaens0e) |
| 4. Work with your SA to secure the technical win and prove that GitLab offers a superior alternative | - Technical demo<br> - Webinars<br> - Other |
| 5. Share customer stories and more | - [Gartner Peer Insights](https://www.gartner.com/reviews/markets)<br> - [Case Studies: Replacing BitBucket](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Replaces%20-%20Bitbucket)<br> - [Case Studies: Replacing Jira](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Replaces%20-%20JIRA)<br> - [Case Studies: Jira Integrations](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&label_name[]=Competitor%2FIntegration%20-%20JIRA) |
| 6. Follow up | - After each call during the trial period, send an [email](https://docs.google.com/document/d/14KDYzQ8HPaH4PW7ndRaBm8O2Z21da6bNp4TRJqnnXmQ/edit#bookmark=id.6tok4lob9uox) with the progress achieved against the success criteria and what should be worked on the following week |


### Part 4: Negotiation / Decision-Making

| Steps | Resources |
| ------ | ------ |
| 1. Build the business case for GitLab (ROI) with the customer in the custom deck for the champion to present to the economic buyer | - Leverage the “How GitLab Does it Better” section in [this doc](https://docs.google.com/document/d/1KDb6wHjanyC805OD1my60wH9Yh5qaq5895KOKDVazXY/edit?usp=sharing) as supporting material |
| 2. Share the Forrester TEI report | - [Forrester TEI report](https://drive.google.com/file/d/1Vi3_InvTs8r6cLvC4gR9bBumlWV5TNvY/view)<br> - [Handbook page](/handbook/marketing/strategic-marketing/analyst-relations/forrester-tei/) |
| 3. Show the value of using GitLab as a single application over other solutions | - Use the [Cost Comparison Calculator](https://about.gitlab.com/calculator/) |

### Part 5: Close the Deal!
